from json import loads, dumps
from socket import socket
from struct import calcsize, pack, unpack


# Implementation of the basic protocol class
class Protocol:
    @staticmethod
    def send(sock, message):
        pass

    @staticmethod
    def recv(sock):
        return None


# Implementation of a simple protocol which prefixes the length of the data
class SimpleProtocol(Protocol):
    @staticmethod
    def send(sock, message):
        if isinstance(sock, socket):
            msg = dumps(message).encode('utf8')
            length = len(msg)
            msg = pack('!L{}s'.format(length), length, msg)
            sock.send(msg)

    @staticmethod
    def recv(sock):
        if isinstance(sock, socket):
            length = sock.recv(calcsize('!L'))
            if len(length) == calcsize('!L'):
                msg = sock.recv(unpack('!L', length)[0])
                return loads(msg.decode('utf8'))
        return None
