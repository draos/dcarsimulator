from math import hypot


# Interpolate between the vectors a and b using the parameter t
def mix(a, b, t):
    return tuple(ak * (1.0 - t) + bk * t for ak, bk in zip(a, b))


# Normalize the vector a
def normalize(ax, ay):
    length = hypot(ax, ay)
    return (ax / length, ay / length) if length > 0.0 else (ax, ay)


# Abstract the track for the simulation
class Track:
    # Initialize the track by setting a start point and a resolution
    def __init__(self, ax, ay, resolution, loop=True):
        self.__points = [(ax, ay)]
        self.__lengths = []
        self.__resolution = resolution
        self.__loop = loop

    @property
    def resolution(self):
        return self.__resolution

    @resolution.setter
    def resolution(self, value):
        if 0.0 < value < 1.0:
            self.__resolution = value

    @property
    def points(self):
        return self.__points

    @property
    def length(self):
        return sum(self.__lengths) if self.__lengths else 0.0

    # Get the information of the track at the length t
    def get_information(self, t):
        if self.__loop:
            t %= self.length
        else:
            t = max(0.0, min(self.length, t))

        # Find the part of the track
        length = 0.0
        for i, part in enumerate(self.__lengths):
            # Check if the given parameter t is within this part of the track
            if t <= length + part:
                # Adjust the parameter to the part of the track
                a, b = self.__points[i], self.__points[i + 1]
                t = (t - length) / part
                point = mix(a, b, t)
                dx, dy = b[0] - a[0], b[1] - a[1]
                return point, normalize(dx, dy), normalize(-dy, dx)
            length += part

        # Last point should be the correct point
        return self.__points[-1], (0, 0), (0, 0)

    # Get the point of the track
    def get_point(self, t):
        return self.get_information(t)[0]

    # Get the direction of the track
    def get_direction(self, t):
        return self.get_information(t)[1]

    # Get the normal of the track
    def get_normal(self, t):
        return self.get_information(t)[2]

    # Adds a new points with the length of the straight line to the last one
    def __add(self, b):
        a = self.__points[-1]
        self.__points.append(tuple(b))
        self.__lengths.append(hypot(b[0] - a[0], b[1] - a[1]))

    # Add the points of the bezier curve to the track using the resolution
    def __bezier(self, points):
        t = self.resolution
        while t < 1.0:
            tmp = points[:]
            # Shrink down the points to 1 element according to de Casteljau
            while len(tmp) > 1:
                tmp = [mix(tmp[i - 1], tmp[i], t) for i in range(1, len(tmp))]
            # The last point is the actual point of the curve
            self.__add(tuple(tmp[0]))
            t += self.resolution
        # Add the last point of the curve
        self.__add(points[-1])

    # Add a simple line between the last point and the new point
    def add_line(self, ax, ay):
        self.__add((ax, ay))

    # Add a cubic bezier curve using the last point as starting point
    def add_cubic(self, ax, ay, bx, by, cx, cy):
        points = self.__points[-1:] + [(ax, ay), (bx, by), (cx, cy)]
        self.__bezier(points)
