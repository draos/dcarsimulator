from messages import MessageID, signal
from unit import UnitThread


# Sensor class which passes the sensoric data to the system
class Sensor(UnitThread):
    def __init__(self, enviroment, bus, name, port, timeout=0.1, interval=1.0):
        UnitThread.__init__(self, name, port, timeout, interval)
        self.__bus = bus
        self.__enviroment = enviroment

    # Input event which sends the data from the enviroment to the bus
    def input(self, client, data):
        if client.name == self.__enviroment:
            self.send(self.__bus, data)


# Actor class which passes the control data to the simulation
class Actor(UnitThread):
    def __init__(self, enviroment, bus, name, port, timeout=0.1, interval=1.0):
        UnitThread.__init__(self, name, port, timeout, interval)
        self.__allowed = set()
        self.__bus = bus
        self.__enviroment = enviroment

    # Allow additional message ids
    def allow(self, *ids):
        self.__allowed.update(ids)

    # Deny message ids
    def deny(self, *ids):
        self.__allowed.difference_update(ids)

    # Check if a message is allowed using the message id
    def is_allowed(self, data):
        if isinstance(data, dict) and 'id' in data:
            return data['id'] in self.__allowed
        return False

    # Input event which sends the whitelisted messaged to the enviroment
    def input(self, client, data):
        if client.name == self.__bus and self.is_allowed(data):
            self.send(self.__enviroment, data)


# Implementation of a basic broadcast channel
class Channel(UnitThread):
    def __init__(self, name, port, timeout=0.1, interval=1.0):
        UnitThread.__init__(self, name, port, timeout, interval)

    # Input event which will broadcast the message
    def input(self, client, data):
        for name in self.clients():
            if client.name != name:
                self.send(name, data)


# Lane departure system
class LDS(UnitThread):
    def __init__(self, bus, name, port, timeout=0.1, interval=1.0):
        UnitThread.__init__(self, name, port, timeout, interval)
        self.__messages = []
        self.__bus = bus

    # Input event which will queue the needed messages
    def input(self, client, data):
        # Only queue lane messages
        if MessageID.is_message(MessageID.Yaw, data):
            yaw = data['yaw']
            self.send(self.__bus, MessageID.build_steering(yaw))
