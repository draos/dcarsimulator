from common import normalize
from component import Actor, Channel, Sensor, LDS
from messages import MessageID, signal
from math import cos, sin, pi, atan2
from unit import UnitThread


# The cars are running in a thread within the main process
class Car(UnitThread):
    def __init__(self, x, y, dx, dy, track, name, base_port, timeout=0.01,
                 interval=1.0):
        UnitThread.__init__(self, name, base_port, timeout, interval)

        self.__track = track
        # The message queue
        self.__messages = []
        # Initialize the component of the car
        self.init_components()
        # Position, velocity and acceleration
        self.__pos = (x, y)
        self.__t = 0.0
        self.__dir = normalize(dx, dy)
        self.__velo = 0.5  # m/s
        self.__accel = 0.0  # m/s²
        # Angular velocity/steering
        self.__steer = 0.0  # 1/s

    @property
    def position(self):
        return tuple(self.__pos)

    @property
    def direction(self):
        return tuple(self.__dir)

    @property
    def velocity(self):
        return self.__velo

    @property
    def angular_velocity(self):
        return self.__steer

    @angular_velocity.setter
    def angular_velocity(self, value):
        self.__steer = value

    @property
    def acceleration(self):
        return self.__accel

    @acceleration.setter
    def acceleration(self, value):
        self.__accel = value

    # Initialize the components of the car
    def init_components(self):
        self.__components = [
            Channel('bus', self.port + 1),
            Sensor(self.unit_name, 'bus', 'sensor', self.port + 2),
            Actor(self.unit_name, 'bus', 'actor', self.port + 3),
            LDS('bus', 'lds', self.port + 4)
        ]

        # Allow acceleration and steering messages to be passed
        self.__components[2].allow(
            MessageID.Acceleration.value,
            MessageID.Steering.value
        )

        # Connect to the sensor and actor
        self.connect('', self.port + 2)
        self.connect('', self.port + 3)

        # Connect every component to the bus
        for comp in self.__components[1:]:
            comp.connect('', self.port + 1)

    # Sends the enviromental messages to the sensors
    def send_messages(self):
        self.send('sensor', MessageID.build_location(*self.__pos))
        self.send('sensor', MessageID.build_velocity(self.__velo))

        # Get the direction of the lane after 1s
        dx, dy = self.__track.get_direction(self.__t + self.__velo)
        # Calculate the angle
        delta = atan2(dy, dx) - atan2(self.__dir[1], self.__dir[0])
        if delta > pi:
            delta -= 2 * pi

        self.send('sensor', MessageID.build_yaw(delta))

    # Process incoming messages
    def process_messages(self):
        # Extract a acceleration message
        counter, msg = signal(MessageID.Acceleration, self.__messages)
        if counter > 0 and isinstance(msg, dict) and 'acceleration' in msg:
            self.__accel = msg['acceleration']

        # Extract a steering message
        counter, msg = signal(MessageID.Steering, self.__messages)
        if counter > 0 and isinstance(msg, dict) and 'steering' in msg:
            self.__steer = msg['steering']

        self.__messages = []

    # Stop the car processes
    def stop(self):
        self.__timer.cancel()
        for comp in self.__components:
            comp.stop()
        super(Car, self).stop()

    # Input event which will be called if data arrives
    def input(self, client, data):
        if client.name == 'actor':
            self.__messages.append(data)

    # Regularly process the messages
    def interval(self):
        # Process the queued messages
        self.process_messages()
        # Send the parameter messages
        self.send_messages()

    # Process event which will be called regularly and updates the car
    def process(self, past):
        # Update the speed
        self.__velo += past * self.__accel

        # Update the direction
        dx, dy = self.__dir
        c, s = cos(self.__steer * past), sin(self.__steer * past)
        self.__dir = normalize(c * dx - s * dy, s * dx + c * dy)

        # Update the position
        dist = past * self.__velo
        self.__t += dist
        self.__pos = self.__track.get_point(self.__t)
        # self.__pos = [p + dist * d for p, d in zip(self.__pos, self.__dir)]
