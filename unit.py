from logging import getLogger
from multiprocessing import Process
from protocol import SimpleProtocol
from select import select
from socket import socket, SOL_SOCKET, SO_REUSEADDR, SHUT_RDWR
from threading import Thread
from time import time


class Client:
    def __init__(self, addr, sock, name):
        self.__addr, self.__sock, self.__name = addr, sock, name

    @property
    def name(self):
        return self.__name

    @property
    def addr(self):
        return self.__addr

    @property
    def sock(self):
        return self.__sock

    def __repr__(self):
        return "<{}: {}>".format(self.name, self.addr)


# Basic networking unit which implements a server and client
class Unit:
    def __init__(self, name, port, timeout=0.1, interval=1.0):
        self.__name, self.__port = name, port
        self.__timeout = timeout
        self.__interval = interval
        # Client structure with client name as key
        self.__clients = {}
        # Lookup table to resolve the sock to the client name
        self.__lookup = {}
        # Get the logger using the unit name
        self.__logger = getLogger(self.__name)

        # Open the server socket
        self.__sock = socket()
        self.__sock.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
        self.__sock.bind(('', self.__port))
        self.__sock.listen(5)

        # Current times
        self.__last_time = self.__last_int = time()

    # ############ Properties ############
    @property
    def unit_name(self):
        return self.__name

    @property
    def port(self):
        return self.__port

    # ############ Client management ############
    # Add a new client
    def add(self, name, addr, sock):
        if sock not in self.__clients:
            self.__clients[name] = Client(addr, sock, name)
            self.__lookup[sock] = name

    # Delete a client
    def delete(self, sock):
        self.__clients = {
            name: client for name, client in self.__clients.items()
            if client.sock != sock
        }

        if sock in self.__lookup:
            del self.__lookup[sock]

    # Return the clients
    def clients(self):
        return list(self.__clients.keys())

    # Close all sockets
    def clear(self):
        # Close all sockets
        for client in self.__clients.values():
            try:
                client.sock.shutdown(SHUT_RDWR)
                client.sock.close()
            except Exception:
                continue

        # Clear the client list
        self.__clients = {}
        self.__lookup = {}
        self.__sock.shutdown(SHUT_RDWR)
        self.__sock.close()

    # ############ Logging functions ############
    def log(self, msg, *args, **kwargs):
        self.__logger.log(msg, *args, **kwargs)

    def error(self, msg, *args, **kwargs):
        self.__logger.error(msg, *args, **kwargs)

    def info(self, msg, *args, **kwargs):
        self.__logger.info(msg, *args, **kwargs)

    # ############ Protocol functions ############
    # Accepts a new connection
    def __accept(self):
        try:
            # Accept the new connection
            sock, addr = self.__sock.accept()
            # The server sends his name first
            SimpleProtocol.send(sock, self.__name)
            # Wait for the client name
            name = SimpleProtocol.recv(sock)
            # Check if the received name is valid
            if not isinstance(name, str):
                return
            # Add the socket to the internal structure
            self.add(name, addr, sock)
        except Exception as e:
            self.error("Accept: %s", e)

    # Connects to another unit
    def connect(self, host, port):
        sock = socket()
        try:
            # Connect to the server
            sock.connect((host, port))
            # Read the server name
            name = SimpleProtocol.recv(sock)
            # Check if the received name is valid
            if not isinstance(name, str):
                return
            # Send the own name to finish the connection process
            SimpleProtocol.send(sock, self.__name)
            # Add the socket to the internal structure
            self.add(name, (host, port), sock)
        except Exception as e:
            self.error("Connect: %s", e)

    # Sends data to another unit
    def send(self, name, data):
        if name in self.__clients:
            sock = self.__clients[name].sock
            try:
                SimpleProtocol.send(sock, data)
            except Exception:
                self.delete(sock)

    # Receive data from another unit
    def __recv(self, sock):
        try:
            return SimpleProtocol.recv(sock)
        except Exception:
            self.delete(sock)
            return None

    # ############ Events ############
    # Gets called on an interval
    def interval(self):
        pass

    # Process event which will be called regularly
    def process(self, past_time):
        pass

    # Input event which will be called if data arrives
    def input(self, client, data):
        pass

    # ############ Server management ############
    # Stops the entire unit
    def stop(self):
        pass

    # Collect all sockets to listen
    def loop(self):
        socks = [self.__sock] + list(self.__lookup)

        # Check for input events
        events = select(socks, [], [], self.__timeout)[0]

        # Handle the input events
        for sock in events:
            # New connection
            if sock == self.__sock:
                self.__accept()

            # Input on the selected socket
            elif sock in self.__lookup:
                data = self.__recv(sock)
                name = self.__lookup[sock]
                if data is not None and name in self.__clients:
                    self.input(self.__clients[name], data)

        # Interval processing
        new_time = time()
        if new_time - self.__last_int > self.__interval:
            self.interval()
            self.__last_int = new_time

        # Normal processing
        self.process(new_time - self.__last_time)
        self.__last_time = new_time


# Unit running in an extra process
class UnitProcess(Process, Unit):
    def __init__(self, name, port, timeout=0.1, interval=1.0):
        Process.__init__(self)
        Unit.__init__(self, name, port, timeout, interval)
        # Start the process
        self.start()

    # Runs the main loop
    def run(self):
        self.log('Starting {} in extra process....'.format(self.unit_name))
        # Run the mainloop until the process gets terminated
        while self.is_alive():
            self.loop()

        # Clear the clients
        self.clear()

    # Stops the entire unit
    def stop(self):
        self.terminate()
        self.join()


# Unit running in an extra thread
class UnitThread(Thread, Unit):
    def __init__(self, name, port, timeout=0.1, interval=1.0):
        Thread.__init__(self)
        Unit.__init__(self, name, port, timeout, interval)
        # Start the thread
        self.start()

    # Returns whether a unit is still running
    def alive(self):
        return self.is_running

    # Runs the main loop
    def run(self):
        self.info('Starting {} with {}....'.format(self.unit_name, self.name))
        self.is_running = True
        # Run the mainloop until the process gets terminated
        while self.is_running:
            self.loop()

        # Clear the clients
        self.clear()

    # Stops the entire unit
    def stop(self):
        self.is_running = False
        self.join()
