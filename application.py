#!/usr/bin/env python3
from car import Car
from common import Track
from logging import basicConfig, getLogger
import OpenGL.GLUT as glut
import OpenGL.GL as gl
import sys


CarWidth = 0.2
CarLength = 0.5


class Application:
    def __init__(self, width, height, base_port):
        self.x, self.y, self.z = 0.0, 0.0, 6.0

        self.__logger = getLogger()

        glut.glutInit(sys.argv)
        mode = glut.GLUT_DOUBLE | glut.GLUT_RGB | glut.GLUT_DEPTH
        glut.glutInitDisplayMode(mode | glut.GLUT_MULTISAMPLE)
        glut.glutInitWindowSize(width, height)
        glut.glutInitWindowPosition(0, 0)
        self.window = glut.glutCreateWindow(bytes("Application", "utf-8"))

        glut.glutDisplayFunc(self.display)
        glut.glutKeyboardFunc(self.keyboard)
        glut.glutIdleFunc(self.idle)

        gl.glClearColor(0.0, 0.0, 0.0, 1.0)
        gl.glViewport(0, 0, width, height)

        self.init_track()
        self.init_cars()

        glut.glutMainLoop()

    # ############ Logging functions ############
    def log(self, msg, *args, **kwargs):
        self.__logger.log(msg, *args, **kwargs)

    def error(self, msg, *args, **kwargs):
        self.__logger.error(msg, *args, **kwargs)

    def info(self, msg, *args, **kwargs):
        self.__logger.info(msg, *args, **kwargs)

    # Stops the child processes
    def stop(self):
        for car in self.cars:
            car.stop()

    # Initialize the cars
    def init_cars(self):
        self.cars = [Car(-4.0, 1.0, 0.0, -1.0, self.track, 'A', 5550)]

    # Initialize the track
    def init_track(self):
        self.track = Track(-4.0, -1.0, 0.1)
        # Top left turn
        self.track.add_cubic(-4.0, -3.0, -3.0, -4.0, -1.0, -4.0)
        self.track.add_line(1.0, -4.0)
        # Top right turn
        self.track.add_cubic(3.0, -4.0, 4.0, -3.0, 4.0, -1.0)
        self.track.add_line(4.0, 1.0)
        # Bottom right turn
        self.track.add_cubic(4.0, 3.0, 3.0, 4.0, 1.0, 4.0)
        self.track.add_line(-1.0, 4.0)
        # Bottom left turn
        self.track.add_cubic(-3.0, 4.0, -4.0, 3.0, -4.0, 1.0)
        self.track.add_line(-4.0, -1.0)

        self.t = 0

    def idle(self):
        glut.glutPostRedisplay()

    # Renders a car
    def render_car(self, position, direction):
        lx, ly = CarLength * direction[0], CarLength * direction[1]
        wx, wy = -CarWidth * direction[1], CarWidth * direction[0]

        gl.glColor3f(1.0, 0.0, 0.0)
        gl.glBegin(gl.GL_LINE_LOOP)
        gl.glVertex2f(position[0] + wx + lx, position[1] + wy + ly)
        gl.glVertex2f(position[0] + wx - lx, position[1] + wy - ly)
        gl.glVertex2f(position[0] - wx - lx, position[1] - wy - ly)
        gl.glVertex2f(position[0] - wx + lx, position[1] - wy + ly)
        gl.glEnd()

        gl.glBegin(gl.GL_LINES)
        gl.glVertex2f(position[0] + wx + 0.8 * lx, position[1] + wy + 0.8 * ly)
        gl.glVertex2f(position[0] - wx + 0.8 * lx, position[1] - wy + 0.8 * ly)
        gl.glEnd()

    # OpenGL drawing function
    def display(self):
        # Clear the current buffer
        gl.glClear(gl.GL_COLOR_BUFFER_BIT)

        # Adjust the camera
        gl.glMatrixMode(gl.GL_PROJECTION)
        gl.glLoadIdentity()
        gl.glOrtho(self.x - self.z, self.x + self.z,
                   self.y - self.z, self.y + self.z, -1.0, 1.0)

        gl.glMatrixMode(gl.GL_MODELVIEW)
        gl.glLoadIdentity()

        # Draw the track as a grey line
        gl.glColor3f(0.8, 0.8, 0.8)
        gl.glBegin(gl.GL_LINE_LOOP)
        for (x, y) in self.track.points:
            gl.glVertex2f(x, y)
        gl.glEnd()

        position, direction, _ = self.track.get_information(self.t)
        for car in self.cars:
            self.render_car(car.position, car.direction)

        self.t += 0.05

        # Swap the buffers
        glut.glutSwapBuffers()

    # Function to handle the keyboard inputs
    def keyboard(self, key, x, y):
        # Escape closes the application
        if key == b"\x1b":
            self.stop()
            exit(0)
        # w, a, s and d will move the viewpoint
        elif key == b"a":
            self.x -= 0.1
        elif key == b"d":
            self.x += 0.1
        elif key == b"w":
            self.y += 0.1
        elif key == b"s":
            self.y -= 0.1
        # + and - will zoom
        elif key == b"-":
            self.z += 0.1
        elif key == b"+":
            self.z = max(0.5, self.z - 0.1)

        glut.glutPostRedisplay()


if __name__ == "__main__":
    basicConfig(
        filename='network.log',
        format='%(asctime)s [%(levelname)s] <%(name)s> | %(message)s'
    )

    Application(500, 500, 5555).stop()
