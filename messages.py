from enum import IntEnum


# Enumeration of currently used message ids
class MessageID(IntEnum):
    Location = 0
    Velocity = 1
    Acceleration = 2
    Steering = 3
    Yaw = 4

    # Checks if a message is a valid message with specific id
    @staticmethod
    def is_message(msg_id, msg):
        if not isinstance(msg_id, MessageID):
            return False
        elif isinstance(msg, dict) and 'id' in msg:
            return msg['id'] in {msg_id, msg_id.value}
        else:
            return False

    # Builds a location message
    @staticmethod
    def build_location(x, y):
        return {'id': MessageID.Location, 'x': x, 'y': y}

    # Builds a lane message
    @staticmethod
    def build_yaw(angle):
        return {'id': MessageID.Yaw, 'yaw': angle}

    # Builds a velocity message
    @staticmethod
    def build_velocity(velocity):
        return {'id': MessageID.Velocity, 'velocity': velocity}

    # Builds a acceleration message
    @staticmethod
    def build_acceleration(acceleration):
        return {'id': MessageID.Acceleration, 'acceleration': acceleration}

    # Builds a steering message
    @staticmethod
    def build_steering(steering):
        return {'id': MessageID.Steering, 'steering': steering}


# Extract messages with the message id and merge them
def signal(msg_id, messages):
    if not isinstance(msg_id, MessageID):
        return 0, None

    # Filter out the matching messages
    counter, final = 0, {'id': msg_id}
    for msg in messages:
        # Check for valid messages
        if MessageID.is_message(msg_id, msg):
            # Append the values to the keys
            for key, value in msg.items():
                if key != 'id':
                    if key not in final:
                        final[key] = [value]
                    else:
                        final[key].append(value)

            # Increment the counter
            counter += 1

    # Merge the values calculating the average
    final = {
        key: value if key == 'id' else sum(value) / counter
        for key, value in final.items()
    }

    return counter, final
