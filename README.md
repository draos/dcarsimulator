# distributed Car Simulator

- Abstracted distributed simulator for organic computing
- Sockets to allow interaction/manipulation with external programs to test the self-x properties

##Requirement:
- PyOpenGL

##Tested
- PyOpenGL 3.1.0
- Python 3.6.3
